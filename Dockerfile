FROM blacklabelops/jobber:aws.v1.2

RUN apk add --no-cache mariadb-client perl-mime-construct ruby && gem install --no-ri dotenv

COPY .env /root/.env
COPY notify-by-email /usr/bin/notify-by-email

ENTRYPOINT ["/sbin/tini", "--", "dotenv", "-f", "/root/.env", "/opt/jobber/docker-entrypoint.sh"]
CMD ["jobberd"]
