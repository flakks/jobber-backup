# CHANGELOG

## v1.0.0
* Initial commit

## v1.0.1
* Rename `JOBS_NOTIFY_CMD` to `NOTIFY_COMMAND`
